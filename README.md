## RMX2020-userdebug 12 SP2A.220405.004 eng.lordsa.20220419.032827 test-keys
- Manufacturer: realme
- Platform: mt6768
- Codename: RMX2020
- Brand: realme
- Flavor: RMX2020-userdebug
- Release Version: 12
- Id: SP2A.220405.004
- Incremental: eng.lordsa.20220419.032827
- Tags: test-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-US
- Screen Density: undefined
- Fingerprint: google/coral/coral:12/SP2A.220405.003/8210211:user/release-keys
- OTA version: 
- Branch: RMX2020-userdebug-12-SP2A.220405.004-eng.lordsa.20220419.032827-test-keys
- Repo: realme_rmx2020_dump_6412


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
